import React from "react";
import { useState, useEffect } from "react";
import produce from "immer";
import { Button, Jumbotron, ListGroup } from "react-bootstrap";

const Names = props => props.data.map(name => <ListGroup.Item>{name.name}</ListGroup.Item>);

export default () => {
  /* Lets user know we are working */
  const initialData = [{ text: "Loading Notes ... " }];
  const [data, setData] = useState(initialData);
  let [nameCount, setNameCount] = useState(0);

  const handleClick = () => {
    const text = document.querySelector("#nameinput").value.trim();
    if (text) {
      /* Update state */
      const nextState = produce(data, draftState => {
        draftState.push({ text });
      });
      document.querySelector("#nameinput").value = "";
      setData(nextState);

      /* Persist Remote */

      fetch("https://baby-names.herokuapp.com/name", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          name: { name: text }
        })
      }).then(()=>{ setNameCount(nameCount +1) });
    }
  };

  /* Remote Storage Hook */
  useEffect(() => {
    fetch("https://baby-names.herokuapp.com/names", {
      method: "GET"
    })
      .then(res => res.json())
      .then(result => {
        setData(result);
      });
  }, [nameCount]);

  return (
    <>
      <Jumbotron>
        <h1>Baby Names!</h1>
        <p>
          (REST API + SPA) that displays a list of baby names that a user submits. 
        </p>
        <p>
          <input
            id="nameinput"
            style={{ width: "80%" }}
            type="text"
            placeholder="Enter a new name"
          />
          <Button variant="primary" onClick={() => handleClick()}>
            Add Name
          </Button>
          <ListGroup>
            <Names data={data} />
		  </ListGroup>
        </p>
      </Jumbotron>
    </>
  );
};

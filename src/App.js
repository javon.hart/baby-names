import React from 'react';
import './App.css';
import BabyNamesComponent from './baby_names_component'


function App() {
  return (
    <div className="App">
      <BabyNamesComponent/>
    </div>
  );
}

export default App;
